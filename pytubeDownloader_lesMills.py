# -*- coding: utf-8 -*-
"""
Created on Fri May  1 21:53:26 2020

@author: Michele
working
"""
import youtube_dl

program = 'bodycombat'
number = '83'
p_name = program + '_' + number

urls = {'bodycombat_83': 'https://embed.vhx.tv/videos/937048?api=1&auth-user-token=eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyNTU1MDI4MywiZXhwIjoxNjIwNTg1NjQ3fQ.BNAtf3KXFs6wYiLAG3ZXZBjAheV61YobdID7Jq16P7M&autoplay=1&back=BODYCOMBAT%20-%2055%20Mins&collection_id=44148&color=00b388&context=https%3A%2F%2Fwatch.lesmillsondemand.com%2Fbodycombat%2Fseason%3A2&live=0&locale=en&playsinline=1&product_id=29964&referrer=https%3A%2F%2Fwatch.lesmillsondemand.com%2Fbodycombat%2Fseason%3A2&sharing=1&title=0&vimeo=1'
        }

name_out = './Documents/Workouts/' + program + '/' + p_name + '.mp4'

ydl_opts = {'outtmpl': name_out}
ydl = youtube_dl.YoutubeDL(ydl_opts)
ydl.download([urls[p_name]])

